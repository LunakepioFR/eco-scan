import * as React from "react";
import "./assets/styles/settings.scss";
import hero from "./assets/images/hero.png";
import Case from "./assets/images/case.png";
import gsap from "gsap";
import logo from "./assets/images/logo.png";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

function App() {
  const headerRef = React.useRef(null);
  const textRef = React.useRef(null);
  const contentRef = React.useRef(null);
  const [isScrolled, setIsScrolled] = React.useState(false);

  React.useEffect(() => {
    const handleScroll = () => {
      const { current } = headerRef;
      if (current) {
        if (window.scrollY > 90) {
          setIsScrolled(true);
        } else {
          setIsScrolled(false);
        }
      }
    };

    const animate = () => {
      handleScroll();
      requestAnimationFrame(animate);
    };

    // Démarre la boucle d'animation
    requestAnimationFrame(animate);

    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  //animate textRef word per word on scroll
  React.useEffect(() => {
    gsap.fromTo(
      textRef.current,
      {
        opacity: 0,
        y: 300,
      },
      {
        opacity: 1,
        y: 0,
        scrollTrigger: {
          trigger: contentRef.current,
          start: "10% 80%",
          end: "25% 20%",
          scrub: true,
        },
      }
    );
  }, []);

  function Menu(){
    return(
    <div className="menu">
    <ul>
      <li className="link">
        <div>Accueil</div>
        <div className="underline"></div>
      </li>
      <li className="link">
        <div>À propos</div>
        <div className="underline"></div>
      </li>
      <li className="link">
        <div>Services</div>
        <div className="underline"></div>
      </li>
      <li className="link">
        <div>Contact</div>
        <div className="underline"></div>
      </li>
    </ul>
  </div>)
  }

  return (
    <div className="App">
      <header ref={headerRef} className={isScrolled ? "bg" : ""}>
        <div className="logo"><img src={logo} alt="logo ecoscan"/></div>
       { window.innerWidth > 1280 ? <Menu/> : null}
        <div className="cta">Télécharger l'app</div>
      </header>

      <div className="hero">
        <div className="col hero-left">
            <div className="hero-text">
              <h1>Un monde meilleur commence par vous</h1>
              <p id="my-text">
                Rejoignez la communauté des personnes qui aiment la nature et
                qui veulent la protéger
              </p>
            </div>
            <div className="hero-cta">
              <button className="btn cta btn-primary">Commencer</button>
              <button className="btn cta btn-secondary">En savoir plus</button>
            </div>
        </div>
        <div className="col hero-right">
          <img src={hero} alt="hero" />
        </div>
      </div>

      <div className="about">
        <div className="row">
          <div className="text">
              <p>
                Dernièrement les émissions mondiales de gaz à effet de serre ont
                augmenté de 23,6 %.
              </p>
              <p>
                Nous devons freiné ce désastre et ça commence avec{" "}
                <span>vous</span>
              </p>
          </div>
        </div>
        <div className="container">
            <div className="col left">
              <p>
                Comment avec EcoSystem vous pouvez freiné ce réchauffement
                climatique ?
              </p>
            </div>
          <div className="col right">
              <div className="item">
                <div className="item-left">01.</div>
                <div className="item-right">
                  <h3>Faites scanné les produits contenus dans votre frigo</h3>
                  <p>
                    Découvrez l'empreinte carbone de chacun de ces produits et
                    le bilan carbone de votre alimentaiton
                  </p>
                </div>
              </div>

              <div className="item">
                <div className="item-left">02.</div>
                <div className="item-right">
                  <h3>
                    Recevez des conseils pour une nutrition saine et plus
                    écofriendly
                  </h3>
                  <p>
                    découvrez comment vous pouvez manger en respectant un peu
                    plus la planète
                  </p>
                </div>
              </div>
              <div className="item">
                <div className="item-left">03.</div>
                <div className="item-right">
                  <h3>
                    Obtenez un bilan carbone précis de votre consommation sur un
                    mois
                  </h3>
                  <p>
                    Découvrez votre impact sur l'environnement et comment vous
                    pouvez le réduire
                  </p>
                </div>
              </div>
              <div className="item">
                <div className="item-left">04.</div>
                <div className="item-right">
                  <h3>Partagez vos résultats avec vos amis et votre famille</h3>
                  <p>
                    Découvrez comment vous pouvez aider vos proches à réduire
                    leurs émissions de CO2
                  </p>
                </div>
              </div>
              <div className="item">
                <div className="item-left">05.</div>
                <div className="item-right">
                  <h3>Obtenez des récompenses pour vos efforts</h3>
                  <p>
                    Faites des économies grâce à des bons d'achats et réductions
                    sur les produits de nos partenaires
                  </p>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div className="slogan" ref={contentRef}>
        <div className="slogan-text">
          <h3 ref={textRef}>
            EcoScan s'engage à réduire les émissions de CO² liées à
            l'alimentation d'au moins 15% d'ici 2024
          </h3>
        </div>
      </div>

      <div className="services">
        <div className="row">
          <div className="text-left">
            <h2>Suivi de l'alimentation</h2>
            <p>Une meilleure alimentation</p>
          </div>
          <div className="text-right">
            <p>
              EcoSystem analyse de nombreuses données pour mesurer la qualité de
              votre alimentation, détecter les problèmes dans celle-ci et vous
              proposer des solutions pour améliorer votre rapport à
              environnement.
            </p>
          </div>
        </div>

        <div className="cards">
          <div className="cards-left">
            <div className="card1">
              <div className="card-item">
                <div className="stat">100</div>
                <div className="stat-text">ClimatoScore</div>
              </div>
              <div className="card-item">
                <div className="stat">􀣉</div>
                <div className="stat-text">Graphique détaillé</div>
              </div>
              <div className="card-item">
                <div className="stat">CO²</div>
                <div className="stat-text">moins d'émissions de CO²</div>
              </div>
              <div className="card-item">
                <div className="stat">€</div>
                <div className="stat-text">
                  économies réalisées grâce à l'application
                </div>
              </div>
              <div className="card-item">
                <div className="stat">􀝖</div>
                <div className="stat-text">Notifications intelligentes</div>
              </div>
            </div>
            <div className="card2">
              <p>graphique ici</p>
            </div>
          </div>
          <div className="cards-right">
            <img src={Case} alt="case" />
          </div>
        </div>
      </div>

      <div className="contact">
        <div className="contact-text">
            <h2>Restez informé</h2>
            <p>
              Inscrivez-vous à notre newsletter pour être informé de nos
              dernières actualités
            </p>
          <div className="contact-form">
              <div className="container">
                <input type="text" placeholder="Inscrivez votre email :" />
                <button>Envoyer</button>
              </div>
          </div>
        </div>
      </div>

      <footer>
        <div className="copyright">
          Copyright © 2022 EcoSystem. All rights reserved.
        </div>
        <div className="menu">
{window.innerWidth > 1280 ? (          <ul>
            <li>RGPD</li>
            <li>CGU</li>
            <li>Politique de confidentialité</li>
            <li>Mentions légales</li>
          </ul>):''}
        </div>
        <div className="country">France</div>
      </footer>
    </div>
  );
}

export default App;
